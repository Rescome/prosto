#include "file_reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>

void read(const char* file_name, wind_rose* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[2];
        while (!file.eof())
        {
            wind_rose* wind = new wind_rose;
            file >> wind->day;
            file >> wind->month;
            file >> wind->derection;
            file >> wind->win;
            array[size++] = wind;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}