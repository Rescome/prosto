#include <iostream>

using namespace std;

#include "wind_rose.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"
#include <iomanip>
void output(wind_rose* wind)
{
    //����� ����
    cout << "****���� ������****" << endl;
    //����
    cout << setw(2) << setfill('0') << wind->day << ":";
    cout << setw(2) << setfill('0') << wind->month << '\n';
    cout << "�����������: " << wind->derection << '\n';
    cout << "��������: " << wind->win << '\n';
    cout << '\n';
}
int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �6. ���� ������\n";
    cout << "�����: ���� ������\n\n";
    cout << "������: 12\n";
    wind_rose* wind[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", wind, size);
		for (int i = 0; i < size; i++)
		{
			output(wind[i]);
		}
		bool (*check_function)(wind_rose*) = NULL;
		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_by_derection; //       
			cout << "*****North, West, NorthWest*****\n\n";
			break;
		case 2:
			check_function = check_by_speed; //       
			cout << "*****�������� ������ 5�/�*****\n\n";
			break;
		default:
			throw "  ";
		}
		if (check_function)
		{
			int new_size;
			wind_rose** filtered = filter(wind, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
		for (int i = 0; i < size; i++)
		{
			delete wind[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	return 0;
}

